/*
Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
Для створення нових елементів необхідно використовувати document.createElement - це найкращий спосіб.
Також можна використовувати innerHTML зі значенням html тегу.
Для додавання елементу використовувати: append, prepend, insertBefore(after)
2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
крок перший знайти елемент:
const element = document.querySelector('.navigation');
крок другий за допомогою функції remove() видалити його;
element.remove();
3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
insertBefore
insertAdjacentElement('beforebegin', elementName)
insertAdjacentElement('afterend', elementName)
insertAdjacentElement('afterend', elementName)
insertAdjacentElement('afterend', elementName)
insertAdjacentText('beforebegin', text)
insertAdjacentText('afterend', text)

Практичні завдання
 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.

 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
 Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.
 */

const a = document.createElement('a');
const footer = document.querySelector('footer');
console.log(footer)
a.innerText = 'Learn More';
a.setAttribute('href', '#');
footer.append(a);

const select = document.createElement('select');
select.setAttribute('id', 'rating');
const main = document.querySelector('main');

for (i=1; i<5; i++) {
    const option = document.createElement('option');
    option.value = i;
    option.textContent = i + " Stars";
    select.append(option);
}

main.prepend(select);